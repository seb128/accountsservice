# SOME DESCRIPTIVE TITLE.
# Copyright (C) YEAR THE PACKAGE'S COPYRIGHT HOLDER
# This file is distributed under the same license as the PACKAGE package.
#
# Translators:
msgid ""
msgstr ""
"Project-Id-Version: accounts service\n"
"Report-Msgid-Bugs-To: https://gitlab.freedesktop.org/accountsservice/"
"accountsservice/issues\n"
"POT-Creation-Date: 2020-05-26 15:24+0000\n"
"PO-Revision-Date: 2021-06-22 08:31+0300\n"
"Last-Translator: Yaron Shahrabani <sh.yaron@gmail.com>\n"
"Language-Team: Hebrew (http://www.transifex.com/freedesktop/accountsservice/"
"language/he/)\n"
"Language: he\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=4; plural=(n == 1 && n % 1 == 0) ? 0 : (n == 2 && n % "
"1 == 0) ? 1: (n % 10 == 0 && n % 1 == 0 && n > 10) ? 2 : 3;\n"
"X-Generator: Poedit 3.0\n"

#: data/org.freedesktop.accounts.policy.in:11
msgid "Change your own user data"
msgstr "החלפת נתוני המשתמש של עצמך"

#: data/org.freedesktop.accounts.policy.in:12
msgid "Authentication is required to change your own user data"
msgstr "נדרש אימות כדי לשנות את נתוני המשתמש של עצמך"

#: data/org.freedesktop.accounts.policy.in:21
msgid "Change your own user password"
msgstr "החלפת ססמת המשתמש של עצמך"

#: data/org.freedesktop.accounts.policy.in:22
msgid "Authentication is required to change your own user password"
msgstr "נדרש אימות כדי לשנות את ססמת המשתמש של עצמך"

#: data/org.freedesktop.accounts.policy.in:31
msgid "Manage user accounts"
msgstr "ניהול חשבונות משתמשים"

#: data/org.freedesktop.accounts.policy.in:32
msgid "Authentication is required to change user data"
msgstr "נדרש אימות כדי לשנות נתוני משתמש"

#: data/org.freedesktop.accounts.policy.in:41
msgid "Change the login screen configuration"
msgstr "עריכת הגדרות מסך הכניסה"

#: data/org.freedesktop.accounts.policy.in:42
msgid "Authentication is required to change the login screen configuration"
msgstr "נדרש אימות כדי לערוך את הגדרות מסך הכניסה"

#: src/main.c:201
msgid "Output version information and exit"
msgstr "הצגת פרטי הגרסה ואז לצאת"

#: src/main.c:202
msgid "Replace existing instance"
msgstr "החלפת עותק קיים"

#: src/main.c:203
msgid "Enable debugging code"
msgstr "הפעלת קוד לניפוי שגיאות"

#: src/main.c:222
msgid ""
"Provides D-Bus interfaces for querying and manipulating\n"
"user account information."
msgstr ""
"מספקת מנשקי D-Bus לתשאול ועריכה של‬‬‬\n"
"‫‫‫פרטי חשבון משתמש."
